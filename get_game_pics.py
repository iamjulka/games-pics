#!/usr/bin/env python3

import argparse
import os.path
import re
import sys
from urllib.error import HTTPError
from urllib.parse import urlparse
import urllib.request
import shutil

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

class WebsiteLooter():
    def __init__(self, **kwds):
        super().__init__(**kwds)

    def set_up(self): pass

class BGGLooter(WebsiteLooter):
    class URLs():
        boardgames = "http://boardgamegeek.com/boardgame/"
        home = "http://boardgamegeek.com/"
        pictures = "http://cf.geekdo-images.com/images/"


    class Locs():
        site_search = By.CSS_SELECTOR, "#sitesearch"
        collection_thumbnail_link = (
            By.CSS_SELECTOR, "#collection .collection_thumbnail>a"
        )
        game_picture_link = By.XPATH, ".//img[@class='img-responsive']/.."
        game_picture = By.XPATH, ".//div[@class='imagecaption']/..//img"


    def __init__(self, driver, **kwds):
        self.driver = driver
        super().__init__(**kwds)

    def download_pic(self, picture_id, filename):
        self.driver.get(self.get_picture_url(picture_id))
        print (driver.get_cookies())
        raise NotImplementedError()

    def find_game(self, game):
        search_bar = self.driver.find_element(
            *self.Locs.site_search
        )
        search_bar.send_keys(game + Keys.ENTER)
        link = self.driver.find_element(
            *self.Locs.collection_thumbnail_link
        )
        return link.get_attribute('href')

    def find_game_id(self, game):
        absolute_url = self.find_game(game)
        path = urlparse(absolute_url).path
        return path.split('/')[2]

    def find_game_pic(self, game):
        self.driver.get(self.URLs.boardgames + self.find_game_id(game))
        link = self.driver.find_element(
            *self.Locs.game_picture_link
        )
        self.driver.get(link.get_attribute('href'))
        img = self.driver.find_element(
            *self.Locs.game_picture
        )
        img_src = img.get_attribute('src')
        img_src_tail = os.path.split(img_src)[1]
        img_regex = re.search("(?P<id>\d+).*(?P<ext>\..+)", img_src_tail)
        return self.get_picture_url(
            img_regex.group('id'),
            img_regex.group('ext')
        )

    def get_picture_url(self, picture_id, extension='.png'):
        return "%spic%s%s" % (self.URLs.pictures, picture_id, extension)

    def set_up(self):
        self.driver.get(self.URLs.home)
        super().set_up()


def download_file(url, filename):
    try:
        with urllib.request.urlopen(url) as response:
            with open(filename, 'wb') as out_file:
                shutil.copyfileobj(response, out_file)
    except HTTPError as e:
        raise HTTPError("%s\nWith url %s" %(e, url))


def parse_cli():
    """Command line arguments are parsed here."""
    parser = argparse.ArgumentParser(
        description = "Download a list of boardgames box pictures"
    )
    parser.add_argument(
        'games', metavar='game', nargs='+', help='a game title')
    parser.add_argument('-o', '--out-dir', dest='out_dir', default='.')
    parser.add_argument('-i', '--in-file', dest='in_file')
    return parser.parse_args()


def main():
    parsed_cli = parse_cli()
    games = parsed_cli.games
    if parsed_cli.in_file:
        with open(parsed_cli.in_file, 'r') as list_file:
            games += [line.rstrip() for line in list_file]
    games = set([g.title() for g in games])
    bgg_looter = BGGLooter(driver = webdriver.Chrome())
    bgg_looter.set_up()
    for game in games:
        game_url = bgg_looter.find_game_pic(game)
        dest = os.path.normpath(os.path.join(
            parsed_cli.out_dir,
            "%s.%s" % (game, game_url.rsplit('.', 1)[1])
        ))
        download_file(game_url, dest)
    return 0

if __name__ == "__main__":
    main()
